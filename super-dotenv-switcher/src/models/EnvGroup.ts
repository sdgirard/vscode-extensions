import { EnvItem } from "./EnvItem";
export class EnvGroup {
  name!: string;
  description?: string;
  envItems?: EnvItem[];
}
