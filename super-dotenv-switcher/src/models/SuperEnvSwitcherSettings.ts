import { workspace, WorkspaceFolder } from "vscode";
import { JsonFileUtility } from "../utils/jsonFileUtility";
import { EnvGroup } from "./EnvGroup";
import * as Constants from "../common/Constants";

export interface ISupperEnvSwitcherSettings {
  LastEnvironment?: string;
  EnvGroups?: EnvGroup[];
}

export class SuperEnvSwitcherSettings {
  public LastEnvironment?: string;
  public EnvGroups?: EnvGroup[];

  private static _instance: SuperEnvSwitcherSettings;

  private readonly _filePath: string;
  public static get Instance(): SuperEnvSwitcherSettings {
    if (!this._instance) {
      this._instance = new SuperEnvSwitcherSettings();
    }
    return this._instance;
  }

  constructor() {
    this._filePath =
      (workspace.workspaceFolders![0] as WorkspaceFolder).uri.fsPath +
      Constants.SuperEnvSwitcherFilePath;

    this.initializeSettings();
  }

  private async initializeSettings() {
    const settings: ISupperEnvSwitcherSettings = await JsonFileUtility.deserializeFromFile(
      this._filePath,
      { LastEnvironment: "", EnvGroups: [] } as ISupperEnvSwitcherSettings
    );
    this.EnvGroups = settings.EnvGroups;
    this.LastEnvironment = settings.LastEnvironment;
  }

  public async save() {
    const saveObj: ISupperEnvSwitcherSettings = {
      LastEnvironment: this.LastEnvironment,
      EnvGroups: this.EnvGroups,
    };
    await JsonFileUtility.serializeToFile(
      this._filePath,
      JSON.stringify(saveObj, null, 2)
    );
  }
}
