import { EReplaceMethod } from "./EReplaceMethod";
export class EnvItem {
    fileName!: string;
    replaceMethod!: EReplaceMethod;
    findPattern!: string;
    replacePattern!: string;
}
