import { QuickPickItem } from "vscode";

export interface IEnvironmentPickItem extends QuickPickItem {
    name: string;
}
