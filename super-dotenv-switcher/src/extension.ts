// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import { commands, ExtensionContext, window } from "vscode";
import { HelloWorldCommand } from "./commands/HelloWorldCommand";
import { SwitchEnvCommand } from "./commands/SwitchEnvCommand";
import { AddEnvSwitchCommand } from "./commands/AddEnvSwitchCommand";

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: ExtensionContext) {
  const switchEnvCommand = new SwitchEnvCommand();
  const helloWorldCommand = new HelloWorldCommand();
  const addEnvSwitchCommand = new AddEnvSwitchCommand();

  context.subscriptions.push(switchEnvCommand);
  context.subscriptions.push(helloWorldCommand);
  context.subscriptions.push(addEnvSwitchCommand);

  context.subscriptions.push(
    commands.registerCommand("super-dotenv-switcher.switch-env", () =>
      switchEnvCommand.run()
    )
  );
  context.subscriptions.push(
    commands.registerCommand("super-dotenv-switcher.helloWorld", () =>
      helloWorldCommand.run()
    )
  );
  context.subscriptions.push(
    commands.registerCommand("super-dotenv-switcher.add-env", () =>
      addEnvSwitchCommand.createEnvironment()
    )
  );

  // // Use the console to output diagnostic information (console.log) and errors (console.error)
  // // This line of code will only be executed once when your extension is activated
  // console.log('Congratulations, your extension "super-dotenv-switcher" is now active!');

  // // The command has been defined in the package.json file
  // // Now provide the implementation of the command with registerCommand
  // // The commandId parameter must match the command field in package.json
  // let disposable = commands.registerCommand('super-dotenv-switcher.helloWorld', () => {
  // 	// The code you place here will be executed every time your command is executed

  // 	// Display a message box to the user
  // 	window.showInformationMessage('Hello World from super dotEnv Switch!');
  // });

  // context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
