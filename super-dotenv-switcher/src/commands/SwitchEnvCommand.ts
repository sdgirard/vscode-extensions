import { window, workspace } from "vscode";
import * as Constants from "../common/Constants";
import { IEnvironmentPickItem } from "../models/IEnvironmentPickItem";
import { SuperEnvSwitcherSettings } from "../models/SuperEnvSwitcherSettings";

export class SwitchEnvCommand {
  private _name: string = "Switch Environment Command";
  private _envName!: string;
  private _settings: SuperEnvSwitcherSettings =
    SuperEnvSwitcherSettings.Instance;

  private static readonly createEnvironmentPickItem: IEnvironmentPickItem = {
    label: `Create Environment`,
    name: Constants.CreateEnvironment,
    description: `Create a new switch dotEnv Configuration.`,
  };

  constructor() {
    this._envName = `Testing`;
  }

  public async run(): Promise<void> {
    console.log("Switch Environments");
    const itemPickList: IEnvironmentPickItem[] = [
      SwitchEnvCommand.createEnvironmentPickItem,
    ];
    let item = await window.showQuickPick(itemPickList, {
      placeHolder: "Select Environment to switch to.",
    });

    if (!item) {
      console.log(`No Environment was selected.`);

      return;
    } else if (item.name === Constants.CreateEnvironment) {
      console.log(`Run create environment command now.`);
      // item = await this.createEnvironment();
    }

    window.showInformationMessage(`Switched to ${this._envName} environment.`);
  }

  public dispose() {
    console.log(`Dispose was called for ${this._name}`);
  }
}
