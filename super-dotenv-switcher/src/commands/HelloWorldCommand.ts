import { window } from 'vscode';

export class HelloWorldCommand {
    _name: string = "Hello World Command";

    public run() {
        // The code you place here will be executed every time your command is executed

        // Display a message box to the user
        window.showInformationMessage('Hello World from super dotEnv Switcher!');
    }

    public dispose() {
        console.log(`Dispose was called for ${this._name}`);
    }

}