import { SuperEnvSwitcherSettings } from "../models/SuperEnvSwitcherSettings";
import { EnvGroup } from "../models/EnvGroup";
import { window } from "vscode";

export class AddEnvSwitchCommand {
  private _name: string = "Add Environment Switch Command";
  private _settings: SuperEnvSwitcherSettings =
    SuperEnvSwitcherSettings.Instance;

  public async createEnvironment(): Promise<void> {
    let newEnvGroup: EnvGroup = new EnvGroup();
    newEnvGroup.name = (await window.showInputBox({
      placeHolder: "Environment Switch Name.",
    })) as string;
    newEnvGroup.description = await window.showInputBox({
      placeHolder: "Description of this Environment Switch",
    });

    if (this._settings.EnvGroups) {
      this._settings.EnvGroups.push(newEnvGroup);
    }

    this._settings.save();
  }

  public dispose() {
    console.log(`Dispose was called for ${this._name}`);
  }
}
