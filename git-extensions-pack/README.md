# Git Essential Extensions Pack
This is an collection of commonly used Git extensions with VS Code.

## Gitlens
https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens

## Git Graph
https://marketplace.visualstudio.com/items?itemName=mhutchie.git-graph

## Git Diff & Merge Tool
https://marketplace.visualstudio.com/items?itemName=david-rickard.git-diff-and-merge-tool

## Git Rebase Shortcuts
https://marketplace.visualstudio.com/items?itemName=trentrand.git-rebase-shortcuts

## ignore "g" it
https://marketplace.visualstudio.com/items?itemName=Andreabbondanza.ignoregit

