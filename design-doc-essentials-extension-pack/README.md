# Design & Documentation Essentials Pack
# PlantUML
https://marketplace.visualstudio.com/items?itemName=jebbs.plantuml
# Markdown All in One
https://marketplace.visualstudio.com/items?itemName=yzhang.markdown-all-in-one
# NX Console
https://marketplace.visualstudio.com/items?itemName=nrwl.angular-console
# Bracket Pair Colorizer 2
https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer-2
# Material Icon Theme
https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme

  